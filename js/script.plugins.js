;(function($){

	"use strict";

	$(document).ready(function(){

		/* ------------------------------------------------
				Form Styler
		------------------------------------------------ */

			if($('select').length){

				$('select').styler();

			}

			if($('input[type="file"]').length){

				$('input[type="file"]').styler();

			}

        /* ------------------------------------------------
				End of Form Styler
		------------------------------------------------ */

		/* ------------------------------------------------
				Simpl Emarquee
		------------------------------------------------ */

			if($('.running_line').length){

				$('.running_line').simplemarquee({
					speed: 100,
		            cycles: Infinity,
		            space: 100,
		            delayBetweenCycles: 2000,
		            handleHover: false,
		            handleResize: false
				});

			}

        /* ------------------------------------------------
				End of Simpl Emarquee
		------------------------------------------------ */

        /* ------------------------------------------------
				Magnific Popup
		------------------------------------------------ */

			$('.photo_gallery').magnificPopup({
				delegate: 'a',
				type: 'image',
				tLoading: 'Loading image #%curr%...',
				mainClass: 'mfp-img-mobile',
				gallery: {
					enabled: true,
					navigateByImgClick: true,
					preload: [0,1] // Will preload 0 - before current, and 1 after the current image
				},
				image: {
					tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
					titleSrc: function(item) {
						return item.el.attr('data-title');
					}
				}
			});

			$('.video_gallery').magnificPopup({
				disableOn: 700,
				type: 'iframe',
				mainClass: 'mfp-fade',
				removalDelay: 160,
				preloader: false,
				fixedContentPos: false
			});

			$('.js_popup_btn').magnificPopup({
				type: 'inline',
				preloader: false
			});

			$('#stock_modal_btn').magnificPopup({
				type: 'inline',
				preloader: false
			});
			

        /* ------------------------------------------------
				End of Magnific Popup
		------------------------------------------------ */


        /* ------------------------------------------------
				particlesJS
		------------------------------------------------ */

		if ($("#particles-js").length) {

			particlesJS('particles-js',
				{
				    "particles": {
				      "number": {
				        "value": 200,
				        "density": {
				          "enable": true,
				          "value_area": 800
				        }
				      },
				      "color": {
				        "value": "#93549f"
				      },
				      "shape": {
				        "type": "circle",
				        "stroke": {
				          "width": 0,
				          "color": "#93549f"
				        },
				        "polygon": {
				          "nb_sides": 5
				        },
				        "image": {
				          "src": "img/github.svg",
				          "width": 100,
				          "height": 100
				        }
				      },
				      "opacity": {
				        "value": 0.5,
				        "random": false,
				        "anim": {
				          "enable": false,
				          "speed": 1,
				          "opacity_min": 0.1,
				          "sync": false
				        }
				      },
				      "size": {
				        "value": 5,
				        "random": true,
				        "anim": {
				          "enable": false,
				          "speed": 40,
				          "size_min": 0.1,
				          "sync": false
				        }
				      },
				      "line_linked": {
				        "enable": true,
				        "distance": 150,
				        "color": "#93549f",
				        "opacity": 0.4,
				        "width": 1
				      },
				      "move": {
				        "enable": true,
				        "speed": 6,
				        "direction": "none",
				        "random": false,
				        "straight": false,
				        "out_mode": "out",
				        "attract": {
				          "enable": false,
				          "rotateX": 600,
				          "rotateY": 1200
				        }
				      }
				    },
				    "interactivity": {
				      "detect_on": "canvas",
				      "events": {
				        "onhover": {
				          "enable": true,
				          "mode": "repulse"
				        },
				        "onclick": {
				          "enable": true,
				          "mode": "push"
				        },
				        "resize": true
				      },
				      "modes": {
				        "grab": {
				          "distance": 400,
				          "line_linked": {
				            "opacity": 1
				          }
				        },
				        "bubble": {
				          "distance": 400,
				          "size": 40,
				          "duration": 2,
				          "opacity": 8,
				          "speed": 3
				        },
				        "repulse": {
				          "distance": 200
				        },
				        "push": {
				          "particles_nb": 4
				        },
				        "remove": {
				          "particles_nb": 2
				        }
				      }
				    },
				    "retina_detect": true,
				    "config_demo": {
				      "hide_card": false,
				      "background_color": "#b61924",
				      "background_image": "",
				      "background_position": "50% 50%",
				      "background_repeat": "no-repeat",
				      "background_size": "cover"
				    }
				}
			);

		};

        /* ------------------------------------------------
				End of particlesJS
		------------------------------------------------ */

	});

	$(window).on('load', function(){
		setTimeout(function(){
			$('#stock_modal_btn').magnificPopup('open')
		},5000)

		/* ------------------------------------------------
				Name pudin
		------------------------------------------------ */

			if($('.first_screan_slider').length){

				var swiper = new Swiper('.first_screan_slider', {
					slidesPerView: 1,
					effect: 'fade',
					pagination: {
						el: '.swiper-pagination',
						clickable: true,
					},
					navigation: {
				        nextEl: '#first_screan_slider_next',
				        prevEl: '#first_screan_slider_prev',
				    },
			    });

			}

			if($('.services_slider').length){

				var servicesSwiper = new Swiper('.services_slider', {
					slidesPerView: 4,
					// slidesPerGroup: 4,
					// spaceBetween: 30,
					on:{
			      		slideChange: function(){
			      			if($(window).width()<768){
			      				$('.services_cont>div').eq(servicesSwiper.activeIndex).show().siblings().hide();
						    	$('.services_pagination>li').eq(servicesSwiper.activeIndex).addClass('current').siblings().removeClass('current');
						    	$('.services_slider').find('[data-id]').removeClass('active');
						    	$('.services_slider').find('[data-id="'+(servicesSwiper.activeIndex+1)+'"]').addClass('active');
			      			}
			      		}
			      	},
					navigation: {
				        nextEl: '#services_slider_next',
				        prevEl: '#services_slider_prev',
				    },
					breakpoints: {
					    // when window width is >= 320px
					    320: {
					      	slidesPerView: 1
					    },

					    // when window width is >= 768px
					    768: {
					      	slidesPerView: 2
					    },
					    // when window width is >= 992px
					    991: {
					      	slidesPerView: 3
					    },
					    // when window width is >= 1250px
					    1249: {
					      	slidesPerView: 4
					    }
					}
			    });

			    $('.services_pagination').on('click', 'a', function(){
			    	
			    	var $this = $(this),
			    		$index = $this.parent('li').index();

			    	if(!$this.closest('li').hasClass('current')){
			    		$('.services_cont>div').eq($index).show().siblings().hide();
				    	$this.closest('li').addClass('current').siblings().removeClass('current');
				    	$('.services_slider').find('[data-id]').removeClass('active');
				    	$('.services_slider').find('[data-id="'+($index+1)+'"]').addClass('active');

				    	servicesSwiper.slideTo($index);
			    	}

			    })

			    $('.services_slider').on('click', '[data-id]', function(){
			    	
			    	var $this = $(this),
			    		$index = $this.attr('data-id');

			    	if(!$this.closest('li').hasClass('current')){
			    		$('.services_cont>div').eq($index-1).show().siblings().hide();
				    	$('.services_pagination').find('li').eq($index-1).addClass('current').siblings().removeClass('current');
				    	$('.services_slider').find('[data-id]').removeClass('active');
				    	$this.addClass('active');
			    	}

			    })

			}


			if($('.teachers_slider').length){

				var swiper = new Swiper('.teachers_slider', {
					slidesPerView: 4,
					slidesPerGroup: 4,
					// spaceBetween: 30,
					// pagination: {
					// 	el: '.swiper-pagination',
					// 	clickable: true,
					// },
					breakpoints: {
					    // when window width is >= 320px
					    320: {
					      	slidesPerView: 1,
							slidesPerGroup: 1
					    },

					    // when window width is >= 768px
					    768: {
					      	slidesPerView: 2,
							slidesPerGroup: 2
					    },
					    // when window width is >= 992px
					    991: {
					      	slidesPerView: 3,
							slidesPerGroup: 3
					    },
					    // when window width is >= 1250px
					    1249: {
					      	slidesPerView: 4,
							slidesPerGroup: 4
					    }
					}
			    });

			}

			if($('.events_slider').length){

				var swiper = new Swiper('.events_slider', {
					slidesPerView: 3,
					slidesPerGroup: 3,
					navigation: {
				        nextEl: '#events_slider_next',
				        prevEl: '#events_slider_prev',
				    },
					breakpoints: {
					    // when window width is >= 320px
					    320: {
					      	slidesPerView: 1,
							slidesPerGroup: 1
					    },

					    // when window width is >= 768px
					    768: {
					      	slidesPerView: 2,
							slidesPerGroup: 2
					    },
					    // when window width is >= 992px
					    991: {
					      	slidesPerView: 3,
							slidesPerGroup: 3
					    },
					}
			    });

			}

			if($('.comment_slider').length){

				var swiper = new Swiper('.comment_slider', {
					slidesPerView: 4,
					slidesPerGroup: 4,
					navigation: {
				        nextEl: '#comment_slider_next',
				        prevEl: '#comment_slider_prev',
				    },
					breakpoints: {
					    // when window width is >= 320px
					    320: {
					      	slidesPerView: 1,
							slidesPerGroup: 1
					    },
					    // when window width is >= 768px
					    768: {
					      	slidesPerView: 2,
							slidesPerGroup: 2
					    },
					    // when window width is >= 992px
					    991: {
					      	slidesPerView: 3,
							slidesPerGroup: 3
					    },
					    // when window width is >= 1250px
					    1249: {
					      	slidesPerView: 4,
							slidesPerGroup: 4
					    }
					}
			    });

			}

        /* ------------------------------------------------
				End of Name pudin
		------------------------------------------------ */

	});

})(jQuery);