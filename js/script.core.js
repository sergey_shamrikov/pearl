;(function($){

	"use strict";

	var Core = {

		DOMReady: function(){

			var self = this;

			self.backToTopBtn({
			    transitionIn: 'bounceInRight',
			    transitionOut: 'bounceOutRight'
			});

			self.box1H();
			if($('.advantages_wrapp').length){
				self.advantages();
			}
			self.tabs();
			self.events();
			self.jqueryExtend();


			if ($(".js_accordion").length) {
			    $(".js_accordion").accordion();
			};

		},

		windowLoad: function(){

			var self = this;

			if($('#first_screan').length){
				self.firstScreanHeight()
			}
		},

		windowResize: function(){

			var self = this;

			if($('#first_screan').length){
				self.firstScreanHeight()
			}

		},

		/**
		**	Back to top
		**/

		backToTopBtn: function(config){

			config = $.extend({
				offset: 350,
				transitionIn: 'bounceInRight',
				transitionOut: 'bounceOutRight'
			}, config);

			var btn = $('<button></button>', {
				class: 'back_to_top animated hide',
				html: '<i class="fa fa-angle-up"></i>'
			}).appendTo($('body')),

			$wd = $(window),
			$html = $('html'),
			$body = $('body');

			$wd.on('scroll.back_to_top', function(){

				if($wd.scrollTop() > config.offset){

					btn.removeClass('hide '+config.transitionOut).addClass(config.transitionIn);

				}
				else{

					btn.removeClass(config.transitionIn).addClass(config.transitionOut);

				}

			});

			btn.on('click', function(){

				$html.add($body).animate({

					scrollTop: 0

				});

			});

	   	},

		/**
		**	Height First Screan Block
		**/

	   	firstScreanHeight: function(){

	   		var wHeight = $(window).height(),
	   			headerHeight = $('#header').outerHeight();

	   		if($('#first_screan').hasClass('title_box')){
	   			$('#first_screan').css({
	   				'max-height':wHeight-headerHeight
	   			});
	   		}
	   		else{
	   			$('#first_screan').height(wHeight-headerHeight);
	   		}

	   	},

	   	/**
	   	**	box1H
	   	**/

	   	box1H: function(){
	   		$('.box1_desc').css({
	   			'height': 'auto'
	   		})
	   		$('.box1_desc').each(function() {
	   			var elH = $(this).find('.box1_title').height();
	   			$(this).css({
		   			'height': elH
		   		});
	   		});
	   	},

	   	/**
	   	**	advantages
	   	**/

	   	advantages: function(){

	   		var wHeight = $(window).height(),
	   			offset = $('.advantages_wrapp').offset().top;

	   		if($(window).scrollTop() > offset-wHeight/1.5){
	   			$('.advantages_wrapp').addClass('active');
	   		}
	   		$(window).on('scroll', function(){
		   		if($(window).scrollTop() > offset-wHeight/1.5){
		   			$('.advantages_wrapp').addClass('active');
		   		}
	   		})

	   	},

		/**
		**	tabs
		**/

	   	tabs: function(){

	   		$('.js_tabs_wrapp').each(function(index, el){

	   			var $active = $(el).find('.js_tabs_list>li.current').length ? $(el).find('.js_tabs_list>li.current') : $(el).find('.js_tabs_list>li:first-child'),
	   				$index = $active.index();

	   			$active.addClass('current').siblings().removeClass('current');
	   			$(el).find('.js_tabs_cont>div').eq($index).show().siblings().hide();
	   		});

	   		$('.js_tabs_wrapp').on('click', '.js_tabs_link', function(){
	   			var $tabs = $(this).closest('.js_tabs_wrapp'),
	   				$index = $(this).closest('li').index();

	   			$(this).closest('li').addClass('current').siblings().removeClass('current');
	   			$tabs.find('.js_tabs_cont>div').eq($index).show().siblings().hide();
	   		})
	   	},

	   	/**
	   	**	events
	   	**/

	   	events: function(){

	   		$('.menu_btn').on('click', function(){

	   			var $this = $(this);
	   			if($('body').hasClass('open_menu')){
	   				$this.removeClass('active');
	   				$('body').removeClass('open_menu');
	   			}
	   			else{
	   				$this.addClass('active');
	   				$('body').addClass('open_menu');
	   			}

	   		});

	   	},

	   	/**
	   	**	jqueryExtend Accordion
	   	**/

	   	jqueryExtend: function() {

	   	    $.fn.extend({

	   	        /**
	   	         ** accordion plugin
	   	         ** @param toggle - set to true, if need to be toggle
	   	         ** @return jquery
	   	         **/

	   	        accordion: function(toggle) {

	   	            return this.each(function() {

	   	                var $this = $(this),
	   	                    active = $this.children('.js_accordion_title.active').length ? $this.children('.js_accordion_title.active') : null;

	   	                $this.children('.js_accordion_title').not(active).next().hide();

	   	                $this.on('click', '.js_accordion_title', function() {

	   	                    var $this = $(this);

	   	                    if (!toggle) {

	   	                        if ($(this).hasClass('active')) {
	   	                            $(this).removeClass('active')
	   	                                .siblings('.js_accordion_title')
	   	                                .removeClass('active')
	   	                                .end()
	   	                                .siblings('.js_accordion_content')
	   	                                .stop()
	   	                                .slideUp();
	   	                        } else {
	   	                            $(this).addClass('active')
	   	                                .siblings('.js_accordion_title')
	   	                                .removeClass('active')
	   	                                .end()
	   	                                .next('.js_accordion_content')
	   	                                .stop()
	   	                                .slideDown(500)
	   	                                .siblings('.js_accordion_content')
	   	                                .stop()
	   	                                .slideUp();
	   	                        }

	   	                    } else {
	   	                        $(this).toggleclass('active').next().stop().slidetoggle();
	   	                    }

	   	                });

	   	            });

	   	        }

	   	    });

	   	},

	}
	/*END Core*/

	$(document).ready(function(){

		Core.DOMReady();

	});

	$(window).on('load', function(){

		Core.windowLoad();

	});

	$(window).on('resize', function(){

		Core.windowResize();

	})

})(jQuery);